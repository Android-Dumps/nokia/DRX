#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_DRX.mk

COMMON_LUNCH_CHOICES := \
    lineage_DRX-user \
    lineage_DRX-userdebug \
    lineage_DRX-eng
