#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from DRX device
$(call inherit-product, device/hmd/DRX/device.mk)

PRODUCT_DEVICE := DRX
PRODUCT_NAME := lineage_DRX
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia 1.3
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="Drax_00WW-user 11 RKQ1.200928.002 00WW_2_370 release-keys"

BUILD_FINGERPRINT := Nokia/Drax_00WW/DRX:11/RKQ1.200928.002/00WW_2_370:user/release-keys
